cmake_minimum_required(VERSION 3.15)

set(vcpkg_root "${CMAKE_SOURCE_DIR}/vcpkg/")
if (NOT EXISTS "${vcpkg_root}/.vcpkg-root")
    execute_process(COMMAND git submodule update --init "${vcpkg_root}")
endif ()
set(
        CMAKE_TOOLCHAIN_FILE "${vcpkg_root}/scripts/buildsystems/vcpkg.cmake"
        CACHE PATH "Path to vcpkg toolchain"
)

project(YTM_Presence)

set(CMAKE_CXX_STANDARD 20)

add_executable(YTM_Presence main.cpp)

add_library(Discord::BaseGameSDK SHARED IMPORTED)
target_include_directories(Discord::BaseGameSDK INTERFACE ${CMAKE_SOURCE_DIR}/lib/include)

# Tests the current platform to determine the file to use

# Windows : Use the .dll and the .dll.lib file
if (WIN32)
    set_target_properties(Discord::BaseGameSDK
            PROPERTIES
            IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/lib/discord_game_sdk.dll
            IMPORTED_IMPLIB ${CMAKE_SOURCE_DIR}/lib/discord_game_sdk.dll.lib
            )

    # Copies discord_game_sdk.dll to build directory in order for the program to work
    add_custom_command(TARGET YTM_Presence
            POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Discord::BaseGameSDK> $<TARGET_FILE_DIR:YTM_Presence>)

    # UNIX (and Linux) : Use the .so file
elseif (UNIX AND NOT APPLE)
    set_target_properties(Discord::BaseGameSDK
            PROPERTIES
            IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/lib/discord_game_sdk.so
            )

    # MacOS : Use the .dylib file
elseif (APPLE)
    set_target_properties(Discord::BaseGameSDK
            PROPERTIES
            IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/lib/discord_game_sdk.dylib
            )
endif ()

# source-only C++ wrapper
file(GLOB SOURCES lib/include/*.cpp)
add_library(DiscordGameSDK STATIC ${SOURCES})
target_link_libraries(DiscordGameSDK PUBLIC Discord::BaseGameSDK)
add_library(Discord::GameSDK ALIAS DiscordGameSDK)

target_link_libraries(YTM_Presence PRIVATE Discord::GameSDK)

find_package(asio CONFIG REQUIRED)
target_link_libraries(YTM_Presence PRIVATE asio asio::asio)
#include <iostream>
#pragma pack(push, 8)
#include <discord.h>
#include <thread>
#pragma pack(pop)

int main() {
    discord::Core *core{};
    auto result = discord::Core::Create(875814437288833085, DiscordCreateFlags_Default, &core);
    discord::Activity activity{};
    activity.SetState("Testing");
    core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {
        std::cout << ((result == discord::Result::Ok) ? "Success" : "Fail");
    });
    while (true) {
       core->RunCallbacks();
    }
    return 0;
}
